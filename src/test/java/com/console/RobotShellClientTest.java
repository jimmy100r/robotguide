package com.console;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import com.model.Direction;


public class RobotShellClientTest {

    RobotShellClient robotShellClient;
    
    @Before
    public void Setup(){
    	robotShellClient= new RobotShellClient();
    }
    
    @Test
    public void testPlace(){
    	robotShellClient.place(1, 2, "NORTH");
    	assertEquals(Direction.NORTH,robotShellClient.getRobot().getF());
    	assertEquals(1,robotShellClient.getRobot().getX());
    	assertEquals(2,robotShellClient.getRobot().getY());
    }
	
    @Test
    public void testPlaceShouldNotCreateRobot(){
    	robotShellClient.place(-1, 2, "NORTH");
    	assertNull(robotShellClient.getRobot());
    	robotShellClient.place(1, -2, "NORTH");
    	assertNull(robotShellClient.getRobot());
    	robotShellClient.place(1, 2, "SOTH");
    	assertNull(robotShellClient.getRobot());
    }

    @Test
    public void testMove(){
    	robotShellClient.place(1, 2, "NORTH");
    	robotShellClient.move();
    	assertEquals(Direction.NORTH,robotShellClient.getRobot().getF());
    	assertEquals(1,robotShellClient.getRobot().getX());
    	assertEquals(3,robotShellClient.getRobot().getY());
    }
	
    @Test
    public void testMoveShouldNotMoveRobot(){
    	robotShellClient.place(2, 5, "NORTH");
    	robotShellClient.move();
    	assertEquals(2, robotShellClient.getRobot().getX());
    	assertEquals(5, robotShellClient.getRobot().getY());
    	assertEquals(Direction.NORTH, robotShellClient.getRobot().getF());
    }
    
    @Test
    public void testRotateLeft(){
    	robotShellClient.place(1, 2, "NORTH");
    	robotShellClient.rotateLeft();
    	assertEquals(Direction.WEST,robotShellClient.getRobot().getF());
    	robotShellClient.rotateLeft();
    	assertEquals(Direction.SOUTH,robotShellClient.getRobot().getF());
    	robotShellClient.rotateLeft();
    	assertEquals(Direction.EAST,robotShellClient.getRobot().getF());
    	robotShellClient.rotateLeft();
    	assertEquals(Direction.NORTH,robotShellClient.getRobot().getF());
    }
	
    @Test
    public void testRotateLeftShouldNotCreateRobot(){
    	robotShellClient.rotateLeft();
    	assertNull(robotShellClient.getRobot());
    }
    
    @Test
    public void testRotateRight(){
    	robotShellClient.place(1, 2, "NORTH");
    	robotShellClient.rotateRight();
    	assertEquals(Direction.EAST,robotShellClient.getRobot().getF());
    	robotShellClient.rotateRight();
    	assertEquals(Direction.SOUTH,robotShellClient.getRobot().getF());
    	robotShellClient.rotateRight();
    	assertEquals(Direction.WEST,robotShellClient.getRobot().getF());
    	robotShellClient.rotateRight();
    	assertEquals(Direction.NORTH,robotShellClient.getRobot().getF());
    }
	
    @Test
    public void testRotateRightShouldNotCreateRobot(){
    	robotShellClient.rotateRight();
    	assertNull(robotShellClient.getRobot());
    }
    
    @Test
    public void testReportShouldReturnNullRobot(){
    	robotShellClient.report();
    	assertNull(robotShellClient.getRobot());
    }
    
}
