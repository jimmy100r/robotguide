package com.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.exception.InvalidDirectionException;

public class DirectionTest {
    
	@Test(expected = InvalidDirectionException.class)
    public void testGetDirectionShouldThrowException() throws InvalidDirectionException{
       Direction.getDirection("NORTHWEST");
    }
	@Test
    public void testGetDirection() throws InvalidDirectionException{
       assertEquals(Direction.NORTH,Direction.getDirection("NORTH"));
    }
}
