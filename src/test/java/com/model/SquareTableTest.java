package com.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.exception.InvalidInputException;


public class SquareTableTest {

	 SquareTable squareTable;
	 
	 @Before
	 public void Setup(){
	 squareTable = new SquareTable(5, 5);
	 }
	 
	 //Test cases for negative co-ordinates
	 @Test
	 public void testIsValidXShouldReturnFalseForNegative() throws InvalidInputException {
		 assertFalse(squareTable.isValidX(-1));
	 }
	 @Test
	 public void testIsValidYShouldReturnFalseForNegative() throws InvalidInputException{
	 assertFalse(squareTable.isValidY(-4));
	 }
	 
	 //Test cases for co-ordinates greater than the table area
	 @Test(expected = InvalidInputException.class)
	 public void testIsValidYShouldThrowInvalidInputException() throws InvalidInputException {
	 squareTable.isValidX(6);
	 }
	 @Test(expected = InvalidInputException.class)
	 public void testIsValidXShouldThrowInvalidInputException() throws InvalidInputException{
	 squareTable.isValidY(9);
	 }
	 
	 //Test cases with valid data
	 @Test
	 public void testIsValidX() throws InvalidInputException{
	 assertTrue(squareTable.isValidX(0));
	 }
	 @Test
	 public void testIsValidY() throws InvalidInputException{
	 assertTrue(squareTable.isValidY(0));
	 }
	 
	 // Tests for invalid table
	 @Test(expected = IllegalArgumentException.class)
	 public void testThrowsIllegalArgumentExceptionForLength() {
	 new SquareTable(-1, 5);
	 }
	 @Test(expected = IllegalArgumentException.class)
	 public void testThrowsIllegalArgumentExceptionForWidth() {
	 new SquareTable(1, -5);
	 }
}
