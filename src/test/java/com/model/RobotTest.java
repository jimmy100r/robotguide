package com.model;


import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


import com.exception.InvalidDirectionException;
import com.exception.InvalidInputException;


public class RobotTest {

	Robot robot;
	
	@Before
	public void Setup(){	
		robot = new Robot();
		robot.setTable(new SquareTable(5,5));
	}

	public RobotTest() throws InvalidDirectionException {
		super();
	}

	@Test(expected = InvalidInputException.class)
	public void testPlaceShouldThrowInvalidInputExceptionForNegativeX() throws InvalidInputException{
		robot.place(-1,2, "NORTH");
		fail("Invalid Input was set in Robot");
	}
	@Test(expected = InvalidInputException.class)
	public void testPlaceShouldThrowInvalidInputExceptionForNegativeY() throws InvalidInputException{
		robot.place(5,-3, "SOUTH");
		fail("Invalid Input was set in Robot");
	}
	@Test(expected = InvalidInputException.class)
	public void testPlaceShouldThrowInvalidInputExceptionForX() throws InvalidInputException{
		robot.place(6,3, "NORTH");
		fail("Invalid Input was set in Robot");
	}
	@Test(expected = InvalidInputException.class)
	public void testPlaceShouldThrowInvalidInputExceptionForY() throws InvalidInputException{
		robot.place(5,7, "SOUTH");
		fail("Invalid Input was set in Robot");
	}
	@Test(expected = InvalidInputException.class)
	public void testPlaceShouldThrowInvalidInputExceptionForF() throws InvalidInputException{
		robot.place(3,3, "NORTHEAST");
		fail("Invalid Input was set in Robot");
	}
	
	@Test
	public void testPlace() throws InvalidInputException{
		robot.place(1,1, "NORTH");
		assertEquals(1, robot.getX());
		assertEquals(1, robot.getY());
		assertEquals(Direction.getDirection("NORTH"), robot.getF());
	}
	
	@Test
	public void testMoveShouldMove() throws InvalidInputException{
		robot.place(1, 1, "NORTH");
		robot.move();
		assertEquals(1, robot.getX());
		assertEquals(2, robot.getY());
	}
	@Test
	public void testMoveShouldNotMove() throws InvalidInputException{
		robot.place(0, 0, "SOUTH");
		robot.move();
		assertEquals(0, robot.getX());
		assertEquals(0, robot.getY());
	}
	
	@Test(expected = InvalidDirectionException.class)
	public void testReportShouldThrowInvalidDirectionException() throws InvalidDirectionException{
		robot.report();
	}
	@Test
	public void testReport() throws InvalidInputException{
		robot.place(1, 2, "NORTH");
		robot.report();
	}

	@Test
	public void testRotateLeft() throws InvalidInputException{
		robot.place(1, 2, "NORTH");
		robot.rotateLeft();
		assertEquals(Direction.WEST, robot.getF());
		robot.rotateLeft();
		assertEquals(Direction.SOUTH, robot.getF());
		robot.rotateLeft();
		assertEquals(Direction.EAST, robot.getF());
		robot.rotateLeft();
		assertEquals(Direction.NORTH, robot.getF());
	}
	@Test(expected=InvalidDirectionException.class)
	public void testRotateLeftShouldThrowInvalidDirectionException() throws InvalidInputException{
		robot.rotateLeft();
	}
	
	@Test
	public void testRotateRight() throws InvalidInputException{
		robot.place(1, 2, "NORTH");
		robot.rotateRight();
		assertEquals(Direction.EAST, robot.getF());
		robot.rotateRight();
		assertEquals(Direction.SOUTH, robot.getF());
		robot.rotateRight();
		assertEquals(Direction.WEST, robot.getF());
		robot.rotateRight();
		assertEquals(Direction.NORTH, robot.getF());
	}
	@Test(expected=InvalidDirectionException.class)
	public void testRotateRightShouldThrowInvalidDirectionException() throws InvalidInputException{
		robot.rotateRight();
	}
}
