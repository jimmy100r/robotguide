package com.model;

import com.exception.InvalidInputException;

public interface Table {

	public boolean isValidY(int y) throws  InvalidInputException;
	public boolean isValidX(int x) throws InvalidInputException;

}
