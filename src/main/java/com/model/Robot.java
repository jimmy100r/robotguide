package com.model;

import java.text.MessageFormat;

import com.exception.InvalidDirectionException;
import com.exception.InvalidInputException;
import com.exception.InvalidTableException;
import com.util.messages.Messages;

 public class Robot {

	private int X,Y;
	private Direction F;
	private Table table;

	public Robot() {
	}
	
	public Robot(int X, int Y, String F, Table table) throws InvalidInputException, InvalidTableException{
		if(table==null)
			throw new InvalidTableException("Table cannot be null" );
		if(!table.isValidX(X))
			throw new InvalidInputException(Integer.toString(X));
		setX(X);
		if(!table.isValidY(Y))
			throw new InvalidInputException(Integer.toString(Y));
		setY(Y);
		setF(Direction.getDirection(F));
		setTable(table);
	}
	
	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}
	
	/**
	 * place Robot on input co-ordinates facing in direction F
	 * @param X,Y (co-ordinates on table) F - direction (NORTH, SOUTH, WEST, EAST)
	 * @return Output Message (String)
	 */
	
	public String place(int X, int Y, String F) throws InvalidInputException{
		if(!table.isValidX(X))
			throw new InvalidInputException("Invalid co-ordinate " + X);
		setX(X);
		if(!table.isValidY(Y))
			throw new InvalidInputException("Invalid co-ordinate " + Y);
		setY(Y);
		setF(Direction.getDirection(F));
		return MessageFormat.format(Messages.getString("PLACE"),getX(),getY(),getF()); 
	}
	
	/**
	 * move Robot in forward direction F by one co-ordinate
	 * @param 
	 * @return Output Message (String)
	 */
	
	public String move(){
		int tempX = getX() , tempY =getY();
		switch (getF()){
		case NORTH : 
			tempY++;
			break;
		case SOUTH : 
			tempY--;
			break;
		case EAST : 
			tempX++;
			break;
		case WEST : 
			tempX--;
			break;
		}
		try{
		if(table.isValidX(tempX) && table.isValidY(tempY)){
			setX(tempX);setY(tempY);
			return MessageFormat.format(Messages.getString("MOVE"),getX(),getY(),getF()); 
		}
		}
		catch (InvalidInputException e){
		return Messages.getString("CANNOT_MOVE");
		}
		return Messages.getString("CANNOT_MOVE");
	}
	
	/**
	 * rotate Robot in left direction
	 * @param 
	 * @return Output Message (String)
	 */
	
	public String rotateLeft() throws InvalidDirectionException{
		if(F==null)
			throw new InvalidDirectionException("Direction cannot be null");
		F = Direction.values()[(F.ordinal()+1)%4];
		return MessageFormat.format(Messages.getString("DIRECTION"),getF()); 
	}

	/**
	 * rotate Robot in right direction
	 * @param 
	 * @return Output Message (String)
	 */
	
	public String rotateRight() throws InvalidDirectionException{
		if(F==null)
			throw new InvalidDirectionException("Direction cannot be null");
		F = Direction.values()[(F.ordinal()+3)%4];
		return MessageFormat.format(Messages.getString("DIRECTION"),getF()); 
	}
	
	/**
	 * report current co-ordinates of Robot 
	 * 	 * @param 
	 * @return Output Message (String)
	 */
	public String report() throws InvalidDirectionException{
		if(getF()==null)
			throw new InvalidDirectionException("Direction cannot be null");
		return MessageFormat.format(Messages.getString("REPORT"),getX(),getY(),getF()); 
	}


	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}

	public Direction getF() {
		return F;
	}

	public void setF(Direction f) {
		F = f;
	}

	}
