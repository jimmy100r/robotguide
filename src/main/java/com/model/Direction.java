package com.model;

import com.exception.InvalidDirectionException;

public enum Direction {
    NORTH, WEST, SOUTH, EAST;
    
    /**
	 * validate and parse the input string and return the corresponding enum direction
	 * @param String direction
	 * @return Enum Direction
	 */
    public static Direction getDirection(String direction)
            throws InvalidDirectionException {
        try {
            return valueOf(direction);
        } catch (IllegalArgumentException e) {
            throw new InvalidDirectionException(direction);
        }
    }

}
