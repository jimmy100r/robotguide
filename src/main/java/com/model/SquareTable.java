package com.model;

import com.exception.InvalidInputException;

/*
 * The table class assumes that the origin will always be at the south-west point of the table
 * 
 */

public class SquareTable implements Table{

	private int MIN_X,MIN_Y;
	private int MAX_X,MAX_Y;
	
	public SquareTable(int length, int width) {
		if(width<0 || length<0)
			throw new IllegalArgumentException("Length or width of the table cannot be negative");
	      MAX_X = length;
	      MAX_Y= width;
	      MIN_X=0;
	      MIN_Y=0;
    }

	/**
	 * check if the Y co-ordinate is valid - is in the range of minimum and maximum co-ordinates of the table
	 * @param Y co-ordinate
	 * @return boolean
	 */
	@Override
	 public boolean isValidY(int y) throws InvalidInputException {
		if(y>MAX_Y)
			throw new InvalidInputException("Co-ordinates cannot be greater than table width. ");
		 return y >= MIN_Y && y <= MAX_Y;
	 }

	/**
	 * check if the X co-ordinate is valid - is in the range of minimum and maximum co-ordinates of the table
	 * @param X co-ordinate
	 * @return boolean
	 */
	@Override
	 public boolean isValidX(int x) throws InvalidInputException{
		if(x>MAX_X)
			throw new InvalidInputException("Co-ordinates cannot be greater than table length. ");
	     return x >= MIN_X && x <= MAX_X;
	 }

}
