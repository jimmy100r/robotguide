package com.exception;

public class InvalidTableException extends Exception {

	 public InvalidTableException(String message) {
	        super(message);
	    }
}
