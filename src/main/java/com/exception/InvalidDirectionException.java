package com.exception;

public class InvalidDirectionException extends InvalidInputException {

	public InvalidDirectionException(String direction) {
		super("Input direction is invalid. Direction cannot be "+direction + ". ");
	}

}
