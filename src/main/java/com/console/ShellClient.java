package com.console;

public interface ShellClient {

	public String place(int X,int Y, String F);
	public String move();
	public String rotateLeft();
	public String rotateRight();
	public String report();

}
