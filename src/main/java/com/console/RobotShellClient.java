package com.console;

import java.text.MessageFormat;

import com.exception.InvalidDirectionException;
import com.exception.InvalidInputException;
import com.exception.InvalidTableException;
import com.model.Robot;
import com.model.SquareTable;
import com.model.Table;
import com.util.messages.Messages;


/**
 * An adapter and a client for a shell/console interface.
 * @author Jay K
 *
 */
public class RobotShellClient implements ShellClient{

	private Robot robot;
	private Table squareTable;
	private static final int LENGTH =5;
	private static final int WIDTH =5;
	
	public RobotShellClient() {
		super();
		squareTable=new SquareTable(LENGTH,WIDTH);
	}

	/**
	 * place Robot on input co-ordinates facing in direction F
	 * @param X,Y (co-ordinates on table) F - direction (NORTH, SOUTH, WEST, EAST)
	 * @return Output Message (String)
	 */
	
	@Override
	public String place(int X,int Y, String F) {
		try {
		if(squareTable.isValidX(X) && squareTable.isValidY(Y)){
			if(robot!=null)
				return robot.place(X,Y,F);
			else
				robot=new Robot(X,Y,F,squareTable);
		} 
		else
			return MessageFormat.format(Messages.getString("OUT_OF_COORDINATES"),LENGTH,WIDTH);
		}
		catch (InvalidInputException e) {
			return e.getMessage() +MessageFormat.format(Messages.getString("OUT_OF_COORDINATES"),LENGTH,WIDTH);
		} catch (InvalidTableException e) {
			return e.getMessage();
		}
		return MessageFormat.format(Messages.getString("NEW_ROBOT"),robot.getX(),robot.getY(),robot.getF()); 
	}				
		
	/**
	 * move Robot forward
	 * @param 
	 * @return Output Message (String)
	 */
	
	@Override
	public String move() {
		if(robot==null)
			return Messages.getString("RUN_PLACE");
		return robot.move();
	}

	/**
	 * rotate Robot in left direction
	 * @param 
	 * @return Output Message (String)
	 */
	@Override
	public String rotateLeft() {
		if(robot==null)
			return Messages.getString("RUN_PLACE");
		try {
			return robot.rotateLeft();
		} catch (InvalidDirectionException e) {
			return e.getMessage();
		}
	}
	
	/**
	 * rotate Robot in right direction
	 * @param 
	 * @return Output Message (String)
	 */
	@Override
	public String rotateRight()  {
		if(robot==null)
			return Messages.getString("RUN_PLACE");
		try {
			return robot.rotateRight();
		} catch (InvalidDirectionException e) {
			return e.getMessage();
		}
	}
	
	/**
	 * report Robot's current co-ordinates and direction
	 * @param 
	 * @return Output Message (String)
	 */
	@Override
	public String report() {
		if(robot==null)
			return Messages.getString("RUN_PLACE");
		try {
			return robot.report();
		} catch (InvalidDirectionException e) {
			return e.getMessage();
		}
	}

	public Robot getRobot() {
		return robot;
	}
}