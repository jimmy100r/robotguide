package com.console;

import java.io.IOException;

import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.ShellFactory;
import com.console.RobotShellClient;

public class RobotConsole 
{
	public static final String QUICK_HELP = ("########   QUICK HELP   ########\n")
			+("##\n")
			+("##\n")
			+("## ENTER==>'?list' for help on list of commands & descriptions\n")
			+("## ENTER==>'?help <cmd>' for help on specific command\n")
			+("##\n")
			+("##\n")
			+("## ENTER==>'PLACE <X> <Y> <F>' to place the robot on (X,Y) facing towards F\n")
			+("## ENTER==>'MOVE' to move the robot forward by one co-ordinate\n")
			+("## ENTER==>'LEFT' to turn the robot towards left by 90 degrees\n")
			+("## ENTER==>'RIGHT' to turn the robot towards left by 90 degrees\n")
			+("## ENTER==>'REPORT' to check the current co-ordinates of the robot\n")
			+("##\n")
			+("##\n")
			+("## ENTER==>'exit' to exit the application\n")
			+("##\n")
			+("## ENTER==>'help' to see this menu again \n");
	
	private RobotShellClient robotShellclient;
	
	@Command(name = "help")
	public String help(){
		return QUICK_HELP;
	}

	@Command (name = "PLACE", abbrev = "place", header = "Placing robot")
	public String place(@Param(name="X")int X,@Param(name="Y")int Y,@Param(name="F")String F) {
		return this.getRobotShellClient().place(X,Y,F.toUpperCase());
	}

	@Command(name = "MOVE", abbrev = "move")
	public String move() {
		return this.getRobotShellClient().move();
	}

	@Command(name = "LEFT", abbrev = "left")
	public String left() {
		return this.getRobotShellClient().rotateLeft();
		}

	@Command(name = "RIGHT", abbrev = "right")
	public String right() {
		return this.getRobotShellClient().rotateRight();
	}

	@Command(name = "REPORT", abbrev = "report")
	public String report() {
		return this.getRobotShellClient().report();
	}

	@Command
	public void exit() { }

	public static void main(String[] args) {

		RobotConsole robotConsole = new RobotConsole();
		robotConsole.setRobotShellClient(new RobotShellClient());

		System.out.println(QUICK_HELP);
		
		
		//Let cliche manage the console.
		try {
			ShellFactory.createConsoleShell("Robot",
					"", robotConsole)
					.commandLoop();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public RobotShellClient getRobotShellClient() {
		return robotShellclient;
	}

	public void setRobotShellClient(RobotShellClient robotShellclient) {
		this.robotShellclient = robotShellclient;
	}

	
}
