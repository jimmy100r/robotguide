##Assumptions & Requirements:
-Application is written and compiled using JDK1.7 

-java / jre 1.7 or greater is installed on linux machine and is defined in path.

##Executing in linux:
- Execute robotGuide-0.0.1-SNAPSHOT-jar-with-dependencies.jar in ./target directory. It can be directly executed to start the RobotGuide in a console. It has a dependency on a 3rd party jar(asg.cliche.jar).

  Use the following command to execute: 
    java -jar robotGuide-0.0.1-SNAPSHOT-jar-with-dependencies.jar

##Console Menu

-Following is the snapshot of RobotGuide console Menu.
  By default the table would initialize with length and width of 5 units. 

-Command Example : 

Robot> PLACE 1 2 NORTH

Placing robot
New robot placed on co-ordinates: (1,2) facing towards NORTH direction

  Above shall place the robot on co-ordinates (1,2) facing in the north direction. 

Main Menu - 

 ########   QUICK HELP   ########


 ENTER==>'?list' for help on list of commands & descriptions
 ENTER==>'?help <cmd>' for help on specific command


 ENTER==>'PLACE <X> <Y> <F>' to place the robot on (X,Y) facing towards F
 ENTER==>'MOVE' to move the robot forward by one co-ordinate
 ENTER==>'LEFT' to turn the robot towards left by 90 degrees
 ENTER==>'RIGHT' to turn the robot towards left by 90 degrees
 ENTER==>'REPORT' to check the current co-ordinates of the robot


 ENTER==>'exit' to exit the application

 ENTER==>'help' to see this menu again

##Build and Setup: 
Use pom.xml at the project root directory to build a new jar.
It creates the robotGuide-0.0.1-SNAPSHOT-jar-with-dependencies.jar in ./target directory.